import {expect } from "chai"; 
import { withoutBody } from "got/dist/source";
import { AuthorController } from "../lib/controllers/author.controller";
const authorSchemas = require('./data/authorSchemas_testData.json');
var chai = require('chai');
chai.use(require('chai-json-schema'));
// globals
let userId;
let authorId;

//////////////

const author = new AuthorController();

describe("Author controller", () => {

    it("Author's Login", async () => {
        let response = await author.userLogin();
        
        expect(response.statusCode, 'Status code 200 expected').to.be.equal(200);
        expect(response.timings.phases.total, 'Response time below 3s').to.be.lessThan(3000);
        expect(response.body).to.be.jsonSchema(authorSchemas.schema_authorLogin);
    });

    it("Check getting User info", async () => {
        let response = await author.getUserId();
        userId = response.body.id;

        expect(response.statusCode, 'Status code 200 expected').to.be.equal(200);
        expect(response.timings.phases.total, 'Response time below 3s').to.be.lessThan(3000);
        expect(response.body).to.be.jsonSchema(authorSchemas.schema_userInfo);
    });

    it("Getting author's basic info", async () => {
        let response = await author.getAuthorInfo();
        authorId = response.body.id;

        expect(response.statusCode, 'Status code 200 expected').to.be.equal(200);
        expect(response.timings.phases.total, 'Response time below 3s').to.be.lessThan(3000);
        expect(response.body).to.be.jsonSchema(authorSchemas.schema_authorInfo);
    });

    it("Setting author's basic info", async () => {
        let authorObj = {
            id: authorId,
            userId: userId,
            avatar: null,
            firstName: "Professor Severus",
            lastName: "Snape",
            job: "Future Defense against the Dark Arts Master",
            location: "Algeria",
            company: "Hogwarts",
            website: "www.hotwitch.com",
            twitter: "https://twitter.com/crazy_potions_master",
            biography: "How I became a Dumbledore's spy"
        };
                
        let response = await author.setAuthorInfo(authorObj);
        
        expect(response.statusCode, 'Status code 200 expected').to.be.equal(200);
        expect(response.timings.phases.total, 'Response time below 3s').to.be.lessThan(3000);
        expect(response.body).to.be.jsonSchema(authorSchemas.schema_authorProfileUpdate);
        
    });

    it("Getting author's overview", async () => {
        let response = await author.getAuthorsOverview(authorId);
        
        expect(response.statusCode, 'Status code 200 expected').to.be.equal(200);
        expect(response.timings.phases.total, 'Response time below 3s').to.be.lessThan(3000);
        expect(response.body).to.be.jsonSchema(authorSchemas.schema_authorOverview);
    });

}
);