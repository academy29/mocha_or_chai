import {expect } from "chai"; 
import { withoutBody } from "got/dist/source";
import { ArticleController } from "../lib/controllers/article.controller";
const articleSchemas = require('./data/articleSchemas_testData.json');
var chai = require('chai');
chai.use(require('chai-json-schema'));
// globals
let userId;
let authorId;
let accessToken = 'eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiI2NjA2YzVjMS01OTJlLTRiYjQtYTAyZi1kMWY1MDljMmUzOWUiLCJpYXQiOjE2NTg1MTUxNjYsImV4cCI6MTY1ODYwMTU2Nn0.3YcuwQSWwsyV0IQ78tobE-93ubUekKj0vgVhD6SQ8zSe4AlAbaHuILBcC8AaBlJo9_tVbpGl3hrphb8Sy30jiQ';
//////////////

const article = new ArticleController();

describe("Article controller", () => {

    it("Login before proceeding", async () => {
        let response = await article.userLogin();
        
        expect(response.statusCode, 'Status code 200 expected').to.be.equal(200);
        expect(response.timings.phases.total, 'Response time below 3s').to.be.lessThan(3000);
    });

    it("Creating new article", async () => { 
        let articleName = 'Test new article';
        let articleText = 'some testing text';
        let response = await article.createNewArticle(articleName, articleText);
        
        expect(response.statusCode, 'Status code 200 expected').to.be.equal(200);
        expect(response.timings.phases.total, 'Response time below 3s').to.be.lessThan(3000);
        expect(response.body).to.be.jsonSchema(articleSchemas.schema_newArticle);

        expect(response.body.name, 'Article Title is not saved correctly').to.be.equal(articleName);
        expect(response.body.text, 'Article text is not saved correctrly').to.be.equal(articleText);
    });

    it("Getting articles of (current) Author", async () => {
        let response = await article.getArticlesByAuthor();

        expect(response.statusCode, 'Status code 200 expected').to.be.equal(200);
        expect(response.timings.phases.total, 'Response time below 3s').to.be.lessThan(3000);
        expect(response.body).to.be.jsonSchema(articleSchemas.schema_allArticlesByAuthor);
    });

    it("Getting article by Id", async () => {
        const articleId = 'ff405e10-5787-41b1-b109-0a9514d58587';

        let response = await article.getArticleById(articleId);

        expect(response.statusCode, 'Status code 200 expected').to.be.equal(200);
        expect(response.timings.phases.total, 'Response time below 3s').to.be.lessThan(3000);
        expect(response.body).to.be.jsonSchema(articleSchemas.schema_articleById);
    });

    it("Posting a comment", async () => {
        const articleId = 'ff405e10-5787-41b1-b109-0a9514d58587';
        const commentText = "Wingardium LeviOsa, not LeviosA!!"
        let response = await article.postArticleComment(articleId, commentText);

        expect(response.statusCode, 'Status code 200 expected').to.be.equal(200);
        expect(response.timings.phases.total, 'Response time below 3s').to.be.lessThan(3000);
        expect(response.body).to.be.jsonSchema(articleSchemas.schema_postComment);

        expect(response.body.text, 'Comment text is not saved correctrly').to.be.equal(commentText);
    });

    it("Getting comments by article id", async () => {
        const articleId = 'a437510a-acf9-4630-a5cc-140e0cde614a';
        
        let response = await article.getCommentsByArticleId(articleId);

        expect(response.statusCode, 'Status code 200 expected').to.be.equal(200);
        expect(response.timings.phases.total, 'Response time below 3s').to.be.lessThan(3000);
        expect(response.body).to.be.jsonSchema(articleSchemas.schema_commentsByArticleId);
        
        for(let i = 0; i<response.body.length; i++){
            expect(response.body[i].articleId, 'Article id of comment expected to correstpond').to.be.equal(articleId);
        }
        
    });

    // Negative tests
    // THIS TEST WILL FAIL
    it("Creating new article with empty title and text", async () => { 
        let articleName = '';
        let articleText = '';
        let response = await article.createNewArticle(articleName, articleText);
        
        expect(response.statusCode, 'Status code 400 expected').to.be.equal(400);
        expect(response.timings.phases.total, 'Response time below 3s').to.be.lessThan(3000);
        expect(response.body).to.be.jsonSchema(articleSchemas.schema_newArticle);

        expect(response.body.name, 'Article Title is not saved correctly').to.be.equal(articleName);
        expect(response.body.text, 'Article text is not saved correctrly').to.be.equal(articleText);
    });

    // another negative test (it will not fail)
    it("Posting a comment for a non-existing article", async () => {
        const articleId = 'a437510a-acf9-4330-a5cc-140e0cde614a';// this article does not exist
        const commentText = "Wingardium LeviOsa, not LeviosA!!"
        let response = await article.postArticleComment(articleId, commentText);

        expect(response.statusCode, 'Status code 404 expected').to.be.equal(404);
        expect(response.timings.phases.total, 'Response time below 3s').to.be.lessThan(3000);
        expect(response.body).to.be.jsonSchema(articleSchemas.schema_articleNotFound);
    });   
});