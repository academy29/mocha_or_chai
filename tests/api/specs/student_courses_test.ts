import {expect } from "chai"; 
import { withoutBody } from "got/dist/source";
import { StudentController } from "../lib/controllers/student.controller";
import { CoursesController } from "../lib/controllers/courses.controller";

const studentSchemas = require('./data/studentSchemas_testData.json');
const coursesSchemas = require('./data/coursesSchemas_testData.json');

var chai = require('chai');
chai.use(require('chai-json-schema'));
// globals
let userId;
//////////////

const student = new StudentController();
const courses = new CoursesController();


describe("Student controller", () => {

    it("Student's Login", async () => {
        let response = await student.userLogin();
        
        courses.setStudentToken(response.body.accessToken);

        expect(response.statusCode, 'Status code 200 expected').to.be.equal(200);
        expect(response.timings.phases.total, 'Response time below 3s').to.be.lessThan(3000);
        expect(response.body).to.be.jsonSchema(studentSchemas.schema_studentLogin);
    });

    it("Getting userId for Student", async () => {
        let response = await student.getUserId();
        userId = response.body.id;

        expect(response.statusCode, 'Status code 200 expected').to.be.equal(200);
        expect(response.timings.phases.total, 'Response time below 3s').to.be.lessThan(3000);
        expect(response.body).to.be.jsonSchema(studentSchemas.schema_userInfo);
    });

    it("Reading all notifications for a student", async () => {
        let response = await student.readAllNotifications(userId);
                
        expect(response.statusCode, 'Status code 200 expected').to.be.equal(200);
        expect(response.timings.phases.total, 'Response time below 3s').to.be.lessThan(3000);
    });
});

describe("Courses controller", () => {

    it("Getting my courses", async () => {
        let response = await courses.getMyCourses();
        
        expect(response.statusCode, 'Status code 200 expected').to.be.equal(200);
        expect(response.timings.phases.total, 'Response time below 3s').to.be.lessThan(3000);
        expect(response.body).to.be.jsonSchema(coursesSchemas.schema_getMyCourses);
    }); 

    it("Getting a course by its Id", async () => {
        let response = await courses.getCourseInfoById('aaa449e4-01e1-49e6-bf54-e8056f58508a');
        
        expect(response.statusCode, 'Status code 200 expected').to.be.equal(200);
        expect(response.timings.phases.total, 'Response time below 3s').to.be.lessThan(3000);
        expect(response.body).to.be.jsonSchema(coursesSchemas.schema_courseInfoById);
    }); 

    // SOME NEGATIVE TESTS FINALLY
    it("Getting a course by a wrong Id", async () => {
        let response = await courses.getCourseInfoById('abrakadabra');
        
        expect(response.statusCode, 'Status code 400 expected').to.be.equal(400);
        expect(response.timings.phases.total, 'Response time below 3s').to.be.lessThan(3000);
    }); 
});





